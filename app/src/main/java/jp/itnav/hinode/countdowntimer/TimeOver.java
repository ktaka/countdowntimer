package jp.itnav.hinode.countdowntimer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

/**
 * Created by itnav on 2014/12/06.
 */
public class TimeOver extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timeover);
    }

    @Override
    public boolean onKeyDown(int KeyCode, KeyEvent event) {
        if (KeyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(TimeOver.this, MyActivity.class);
            startActivity(intent);
            TimeOver.this.finish();

            return true;
        }
        return false;
    }
}
