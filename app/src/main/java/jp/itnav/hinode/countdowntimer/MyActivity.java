package jp.itnav.hinode.countdowntimer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Random;


public class MyActivity extends Activity {
    final MyCountDownTimer cdt1 = new MyCountDownTimer(90000, 100);
    int number1 = -1;
    int number2 = -1;
    int number3 = -1;


    ImageView imageView1;
    ImageView imageView2;
    ImageView imageView3;

    Button retryButton;

    Random random;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        cdt1.start();

        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView2 = (ImageView) findViewById(R.id.imageView2);
        imageView3 = (ImageView) findViewById(R.id.imageView3);
        retryButton = (Button)findViewById(R.id.retryButton);

        random = new Random();

        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                retryButton.setVisibility(View.INVISIBLE);
                number1 = number2 = number3 = -1;
                imageView1.setEnabled(true);
                imageView2.setEnabled(true);
                imageView3.setEnabled(true);
            }
        });

        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number1 = -2;
                checkSlotMatching();
                view.setEnabled(false);
            }
        });
        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number2 = -2;
                checkSlotMatching();
                view.setEnabled(false);
            }
        });
        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number3 = -2;
                checkSlotMatching();
                view.setEnabled(false);
            }
        });
    }

    private void checkSlotMatching() {
        if (number1 == -2 && number2 == -2 && number3 == -2) {
            Integer tag_a = (Integer) imageView1.getTag();
            Integer tag_b = (Integer) imageView2.getTag();
            Integer tag_c = (Integer) imageView3.getTag();

            if (tag_a == tag_b && tag_a == tag_c) {
                Intent intent = new Intent(MyActivity.this, Clear.class);
                startActivity(intent);
                cdt1.cancel();
                MyActivity.this.finish();
            } else {
                retryButton.setVisibility(View.VISIBLE);
            }
        }
    }

    public class MyCountDownTimer extends CountDownTimer {

        int i = 0;
        int n = 1;
        int t = 2;
        int e = 3;
        int g = 4;
        Integer tag0 = new Integer(i);
        Integer tag1 = new Integer(n);
        Integer tag2 = new Integer(t);
        Integer tag3 = new Integer(e);
        Integer tag4 = new Integer(g);


        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);

        }

        @Override
        public void onFinish() {
            Intent intent = new Intent(MyActivity.this, TimeOver.class);
            startActivity(intent);
            MyActivity.this.finish();

        }

        private void changeSlotImage(ImageView imgView, int imgNumber) {
            switch (imgNumber) {
                case 0:
                    imgView.setImageResource(R.drawable.ic_launcher);
                    imgView.setTag(tag0);
                    break;
                case 1:
                    imgView.setImageResource(R.drawable.bomb1);
                    imgView.setTag(tag1);
                    break;
                case 2:
                    imgView.setImageResource(R.drawable.bomb_slot);
                    imgView.setTag(tag2);
                    break;
                case 3:
                    imgView.setImageResource(R.drawable.icon2);
                    imgView.setTag(tag3);
                    break;
                case 4:
                    imgView.setImageResource(R.drawable.ishinomaki);
                    imgView.setTag(tag4);
            }
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (number1 > -2) {
                number1 = random.nextInt(5);
                changeSlotImage(imageView1, number1);
            }
            if (number2 > -2) {
                number2 = random.nextInt(5);
                changeSlotImage(imageView2, number2);

            }
            if (number3 > -2) {
                number3 = random.nextInt(5);
                changeSlotImage(imageView3, number3);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



}
